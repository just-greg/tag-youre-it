## Software Project

Use the `protected` branch for the latest and greatest stable release: https://gitlab.com/just-greg/tag-youre-it/-/tree/protected

### Installation

Run the following script:

```
curl https://gitlab.com/just-greg/tag-youre-it/-/raw/main/install.sh | sudo bash
```
